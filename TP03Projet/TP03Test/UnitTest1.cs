using System;
using Xunit;

using TP03Console;

namespace TP03Test
{
    public class UnitTest1
    {
        [Fact]
        public void TestCalculAireCarre()
        {
            Assert.Equal(4, Program.CalculAireCarre(2));
            Assert.Equal(0, Program.CalculAireCarre(0));
            Assert.Equal(1, Program.CalculAireCarre(1));
        }

        [Fact]
        public void TestCalculAireRectangle()
        {
            Assert.Equal(0, Program.CalculAireRectangle(2,0));
            Assert.Equal(21, Program.CalculAireRectangle(7,3));
        }

        [Fact]
        public void TestCalculAireTriangle()
        {
            Assert.Equal(0, Program.CalculAireTriangle(2,0));
            Assert.Equal(12.5, Program.CalculAireTriangle(5,5));
        }
    }
}
