﻿using System;

namespace TP03Console
{
    public class Program
    {
        public static float CalculAireCarre(float CoteCarre)
        {
            return CoteCarre * CoteCarre;
        }

        public static float CalculAireRectangle(float LongueurRectangle, float LargeurRectangle)
        {
            return LargeurRectangle * LongueurRectangle;
        }

        public static float CalculAireTriangle(float BaseTriangle, float HauteurTriangle)
        {
            return (BaseTriangle * HauteurTriangle)/2;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
